//
//  SearchDetailViewController.swift
//  2_debug_study_swift
//
//  Created by 鶴本 幸大 on 2016/12/06.
//  Copyright © 2016年 鶴本 幸大. All rights reserved.
//

import UIKit

class SearchDetailViewController: UIViewController {

    var items : NSArray!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "検索結果"
        tableView.rowHeight = 100
        let nib = UINib(nibName: "SongDetailCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "SongDetailCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : SongDetailCell = tableView.dequeueReusableCell(withIdentifier: "SongDetailCell") as! SongDetailCell
        
        if let result = items?[indexPath.row] as! Dictionary<String, Any>? {
            cell.artistName = result["artistName"] as! String
            cell.songName = result["trackName"] as! String
            
            let requestUrl = URL(string: result["artworkUrl100"] as! String)
            let config = URLSessionConfiguration.default
            let session = URLSession(configuration: config)
            let task = session.downloadTask(with: requestUrl!, completionHandler: { (location, response, error) in
                do {
                    let imageData : NSData = try NSData(contentsOf: location!,options: NSData.ReadingOptions.mappedIfSafe);
                    
                    DispatchQueue.main.async {
                        cell.artistIcon = UIImage(data:imageData as Data)!;
                    }
                } catch {
                    // エラー時
                }
            })
            task.resume()
            
        }
        
        
        return cell
    }
    
    func tableView(_ table: UITableView, didSelectRowAtIndexPath indexPath:NSIndexPath) {
        table.deselectRow(at: indexPath as IndexPath, animated: true)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
        
        if let result = items?[indexPath.row] as! Dictionary<String, Any>? {
            vc.previewUrl = result["previewUrl"] as! String
            vc.trackName = result["trackName"] as! String
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
