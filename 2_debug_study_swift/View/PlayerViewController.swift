//
//  PlayerViewController.swift
//  2_debug_study_swift
//
//  Created by 鶴本 幸大 on 2016/12/06.
//  Copyright © 2016年 鶴本 幸大. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class PlayerViewController: AVPlayerViewController {

    var trackName : String!
    var previewUrl : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = trackName
        
        if let previewUrl = previewUrl {
            player = AVPlayer(url: URL(string: previewUrl)!)
            player?.play()
        }
    }
    
}
