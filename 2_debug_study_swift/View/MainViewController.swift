//
//  MainViewController.swift
//  2_debug_study_swift
//
//  Created by 鶴本 幸大 on 2016/12/07.
//  Copyright © 2016年 鶴本 幸大. All rights reserved.
//

import UIKit

class MainViewController: UIViewController , URLSessionDataDelegate , URLSessionTaskDelegate {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var searchSegmentControl: UISegmentedControl!
    
    @IBOutlet weak var explanationText: UILabel!
    
    var receiveData : NSMutableData!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func pushSearch(_ sender: Any) {
        // itunesAPI
        var str : String = ""
        if (self.searchSegmentControl.selectedSegmentIndex == 0) {
            str = String(format: "https://itunes.apple.com/search?term=%@&country=JP&lang=ja_jp&media=music&entity=song&attribute=artistTerm&limit=50", arguments: [self.textField.text!])
        } else {
            str = String(format: "https://itunes.apple.com/search?term=%@&country=JP&lang=ja_jp&media=music&entity=song&attribute=songTerm&limit=50", arguments: [self.textField.text!])
        }
        
        str = str.addingPercentEscapes(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
        
        let requestUrl = URL(string: str)
        let config = URLSessionConfiguration.default
        config.requestCachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalCacheData
        let session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
        let req = URLRequest(url: requestUrl!)
        let task = session.dataTask(with: req)
        task.resume()
        
    }
    
    // データ受け取り初期処理
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        NSLog("データ受け取り初期処理")
        self.receiveData = NSMutableData()
        completionHandler(URLSession.ResponseDisposition.allow)
    }
    
    // 受信の度に実行
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        NSLog("受信の度に実行")
        self.receiveData.append(data)
    }
    
    // 完了
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        NSLog("通信完了")
        
        if (error != nil) {
            // Httpリクエスト失敗
            DispatchQueue.main.async {
                let alert: UIAlertController = UIAlertController(title: "エラー",
                                                                 message: "通信エラーが発生しました。\n再度、ご確認ください。",
                                                                 preferredStyle:  UIAlertControllerStyle.alert)
                
                let defaultAction: UIAlertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler:nil)
                alert.addAction(defaultAction)
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            
            // Httpリクエスト成功
            DispatchQueue.main.async {
                do {
                    // データ解析
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "SearchDetailViewController") as! SearchDetailViewController
                    let dict = try JSONSerialization.jsonObject(with: self.receiveData! as Data) as! [String : Any]
                    vc.items = dict["results"] as? [[String: AnyObject]] as NSArray!
                    self.navigationController?.pushViewController(vc, animated: true)
                } catch {
                    // エラー処理

                    }
                    
                }
            }
        }
    }

