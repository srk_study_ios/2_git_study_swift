
//
//  SongDetailCell.swift
//  2_debug_study_swift
//
//  Created by 鶴本 幸大 on 2016/12/07.
//  Copyright © 2016年 鶴本 幸大. All rights reserved.
//

import UIKit

class SongDetailCell: UITableViewCell {

    @IBOutlet weak var artistImageView: UIImageView!
    
    @IBOutlet weak var songNameLabel: UILabel!
    
    @IBOutlet weak var artistNameLabel: UILabel!
    
    var artistName: String {
        get { return self.artistName }
        set { artistNameLabel.text = newValue }
    }
    
    var songName: String {
        get { return self.songName }
        set { songNameLabel.text = newValue }
    }

    var artistIcon: UIImage {
        get { return self.artistIcon }
        set { artistImageView.image = newValue }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
